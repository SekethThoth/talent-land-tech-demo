# Talent-Land-Tech-Demo

A DevOps Tech Demo using Terraform & AWS

## Get Started

1. First of all clone this repository and have your own repo uploaded to your GitLab account.

2. Make sure to also set the following variables in your GitLab Repository: Settings -> CI/CD -> Variables
	```
	AWS_AK -> Fill with your AWS Access Key ID and "mask" variable.
	AWS_SAK -> Fill with your AWS Secret Access Key and "mask" variable.
	PIPELINE_ACTION -> Fill with "ignore" for Applying infrastructure, and "destroy" for deleting it.
	```

3. First make sure you have Terraform installed in your local machine `terraform --version` should output the version number like this:
	
	 `Terraform v1.2.5`

3. Make sure you have AWS CLI & CLI Keys in your `default` profile. `aws --version` should output the version number like this:

	`aws-cli/2.1.0 Python/3.7.4 Darwin/19.6.0 exe/x86_64`

4. Make sure you have Ansible installed in your local machine `ansible --version` should output the version number like this:

	`ansible 2.10.3`

5. Make sure you have an SSH Key created in EC2 -> Key Pairs and have downloaded this Key Pair and it's in `~/.ssh` for this example I have created my own key named `talent-land` which is not available for you. Also make sure the EC2 instance in `ec2.tf` contains the name of this key pair.

6. Create the `backend` S3 State Infrastructure in the `/backend` directory by going into the directory in your terminal and running `terraform init` and then `terraform plan` and `terraform apply -auto-approve`. Make sure you change Bucket and DynamoDB Table names beforehand.

7. Copy the S3 Bucket and DynamoDB Table Names and paste them into the `makefile` and the `.gitlab-ci.yml` and commit your changes into the repository so the Pipeline Picks them up if you want to test the GitLab CI Pipeline deployment, if you do, the steps 8 to 9 are handled by the Pipeline `plan` and `apply` stages.

8. Run the `make dev` command and wait for the Terraform Plan Output

9. When finished and Terraform Plan Output is available, run `make apply` and wait for the infrastructure to be provisioned.

10. When the infrastructure has finished provisioning let's copy the Terraform Output called `Web_Server_Public_IP` and paste it into the following file: `configuration-management/inventory` and paste it where you see `[IP_ADDRESS]`, also change the place where you see this part: `ansible_ssh_private_key_file=~/.ssh/talent-land.pem` and replace the last part with the directory and name of your SSH key for your instance.

11. Navigate into `configuration-management` and let's run `ansible-playbook install-apache.yml` and wait for the outpu t to indicate that the installing is complete.

12. Now navigate to `http://IP_ADDRESS` and check to validate that your Apache Server is up and running.

13. We are done!

---

