terraform {
  backend "s3" {
    key     = "terraform-cicd.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}