module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket                  = "sre-training-100760328668"
  restrict_public_buckets = true
  acl                     = "private"

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning = {
    enabled = true
  }

  tags = {
    Environment = "Dev"
  }

}