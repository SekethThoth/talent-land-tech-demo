module "dynamodb_table" {
  source = "terraform-aws-modules/dynamodb-table/aws"

  name                           = "sre-training-100760328668"
  billing_mode                   = "PAY_PER_REQUEST"
  hash_key                       = "LockID"
  server_side_encryption_enabled = true

  attributes = [
    {
      name = "LockID"
      type = "S"
    }
  ]

  tags = {
    Environment = "Dev"
  }
}