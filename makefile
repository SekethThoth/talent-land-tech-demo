#!/bin/bash -x

DEV_ACCOUNT_ID=100760328668
QA_ACCOUNT_ID=
PROD_ACCOUNT_ID=

dev:
	make init && \
	make workspace_dev && \
	make plan_dev

init:
	terraform init -input=false -backend-config="bucket=talent-land-state-bucket-${DEV_ACCOUNT_ID}" -backend-config="dynamodb_table=terraform-test-locks-table-${DEV_ACCOUNT_ID}"

lint:
	terraform fmt -recursive && terraform validate

workspace_dev:
	terraform workspace select dev || terraform workspace new dev

refresh_state_dev:
	terraform refresh -var-file tfvars/generic.tfvars -var "region=us-east-1" -input=false

plan_dev:
	make lint && \
	terraform plan -var-file tfvars/generic.tfvars -var "region=us-east-1" -input=false -out=tfplan

apply_dev:
	make lint && \
	terraform apply -input=false -auto-approve tfplan

destroy_dev:
	make lint && \
	terraform destroy -var-file tfvars/generic.tfvars -var "region=us-east-1" -input=false -auto-approve -state=tfplan